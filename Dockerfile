FROM ubuntu:20.04

RUN apt-get update && apt-get install -y gnupg \
    && echo "deb http://repo.pritunl.com/stable/apt focal main" > /etc/apt/sources.list.d/pritunl.list \
    && apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 7568D9BB55FF9E5287D586017AE645C0CF8E292A \
    && apt-get update \
    && apt-get install -y pritunl mongodb-server supervisor \
    && mkdir -p /data/db \
    && rm -rf /var/lib/apt/lists/*

COPY supervisor/mongo.conf /etc/supervisor/conf.d/
COPY supervisor/pritunl.conf /etc/supervisor/conf.d/
COPY supervisor/supervisord.conf /etc/supervisor
COPY pritunl.conf /etc/pritunl.conf
COPY entrypoint.sh /entrypoint.sh

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
