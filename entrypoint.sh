#!/usr/bin/env bash

# Exit when any command fails
set -e

mkdir -p /dev/net
mknod /dev/net/tun c 10 200

exec /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf