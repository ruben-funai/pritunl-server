# Pritunl Server

## Launch Server

```bash
docker-compose up -d
```

## Get Default Password

```bash
docker-compose exec pritunl pritunl default-password
```